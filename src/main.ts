import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from './config/config.service';
import { NestExpressApplication } from '@nestjs/platform-express';
import { swaggerConfig } from './shared/swagger';

/**
 * bootstrap() - This method is used to server creation
 * swagger config
 * listen port
 */
async function bootstrap(): Promise<any> {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  //configure swagger
  swaggerConfig(app);

  //entry point
  const configService = new ConfigService();
  await app.listen(configService.get('port'));
}
bootstrap();
