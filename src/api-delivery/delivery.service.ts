import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Delivery } from './schema/delivery.schema';

@Injectable()
export class DeliveryService {
  constructor(@InjectModel(Delivery.name) private readonly deliveryModel: Model<Delivery>) {}
  async getDelivery(): Promise<any> {
    const result = await this.deliveryModel.find();
    return result;
  }
}
