import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Delivery extends Document {
  @Prop()
  title: string;

  @Prop()
  content: string;

  constructor() {
    super();
    this.title = '';
    this.content = '';
  }
}

export const DeliverySchema = SchemaFactory.createForClass(Delivery);
