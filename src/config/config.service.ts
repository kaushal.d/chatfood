import 'dotenv/config';

export class ConfigService {
  MONGODB_URI: string;
  port: number;
  private readonly envConfig: { [key: string]: any };

  constructor() {
    this.MONGODB_URI = '';
    this.port = 0;
    this.envConfig = {};
    this.envConfig = {
      MONGODB_URI: 'mongodb://' + process.env.MONGODB_HOST + '/' + process.env.MONGODB_DATABASE,
      port: process.env.PORT,
    };
  }

  get(key: string): any {
    return this.envConfig[key];
  }
}
