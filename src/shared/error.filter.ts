import { Catch, ExceptionFilter, HttpException, ArgumentsHost } from '@nestjs/common';

/**
 * Error response wrapper.
 */
@Catch()
export class errorFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost): any {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const stauts = exception.getStatus();

    const errorResponse = {
      code: stauts,
      message: exception.message || null,
    };

    response.status(stauts).json(errorResponse);
  }
}
