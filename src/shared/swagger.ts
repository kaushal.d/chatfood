import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

export function swaggerConfig(app: any): any {
  const options = new DocumentBuilder()
    .setTitle('Delivery Service Api')
    .setDescription('The delivery service helps to place, cancel and track delivery for all the services providers')
    .setVersion('1.0')
    .addTag('delivery')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
}
