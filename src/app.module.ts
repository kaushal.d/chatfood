import { Module } from '@nestjs/common';
import { DeliveryModule } from './api-delivery/delivery.module';
import { ConfigModule } from '@nestjs/config';
import { APP_FILTER } from '@nestjs/core';
import { errorFilter } from './shared/error.filter';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [
    DeliveryModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    DatabaseModule,
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: errorFilter,
    },
  ],
})
export class AppModule {}
