# service-delivery-api

# command to run the project
 > cd ../service-delivery-api
 > npm run start

### [High Level Architecture Design](https://app.lucidchart.com/documents/embeddedchart/827dbffa-f23d-4246-8f6f-211a07a9d697)


# command to run the project
 > cd ../service-delivery-api
 > npm run start

# Project Description
 > This project helps to connect the client to the different delivery service provider by a single API
 > When the client calls the API, the request body we have the following details
    - Key to validate the client
    - External account id to know which service provider and the respective account to be used
    - Data related to the delivery